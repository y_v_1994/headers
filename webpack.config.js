const path = require('path');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');

const clients = ['default', 'euronews', 'dallasnews', 'dailymail'];
const HtmlWebpackPluginConfigs = [];
const entries = {};

clients.forEach((client) => {
    HtmlWebpackPluginConfigs.push(new HtmlWebpackPlugin({
        template: `./app/clients/${client}/index.html`,
        chunks: [`${client}`],
        filename: `/app/clients/${client}/index.html`,
        inject: 'head',
    }));
    entries[client] = `./app/clients/${client}/${client}.js`
});

module.exports = {
    entry: entries,
    output: {
        path: path.resolve(__dirname, 'dist'),
        publicPath: '/',
        filename: 'app/clients/[name]/[name].js'
    },
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: {
                    loader: "babel-loader"
                }
            },
            {
                test: /\.scss$/,
                use: ExtractTextPlugin.extract(
                    {
                        fallback: 'style-loader',
                        use: ['css-loader', 'sass-loader']
                    })
            },
            {
                test: /\.svg$/,
                loader: 'raw-loader'
            },
            {
                test: /\.(png|jpe?g|gif)$/i,
                loader: 'file-loader',
                options: {
                    name: '[path][name].[ext]',
                },
            }
        ]
    },
    plugins: [
        ...HtmlWebpackPluginConfigs,
        new ExtractTextPlugin({filename: 'app/clients/[name]/[name].css'})
    ],
    devServer: {
        contentBase: path.join(__dirname, 'dist'),
            compress: true,
            port: 9000
    }
}