import EventBus from "eventbusjs";

export class I18nService {

    constructor() {
        if (!!I18nService.instance) {
            return I18nService.instance;
        }
        I18nService.instance = this;
        this.initState = {lang: "en", langPack: {LOGIN: "login", GAME: "game"}};
        this.state = this.initState;
        console.log("i18nService");
        EventBus.addEventListener("CHANGE_LANG_EVENT", (event, args) => {
            if (args) {
                this.state = args;
                EventBus.dispatch("RERENDER_EVENT");
            }
        });
        return this;
    }

    static getInstance() {
        return new I18nService();
    }

    getState() {
        return this.state;
    }

}

I18nService.instance = null;