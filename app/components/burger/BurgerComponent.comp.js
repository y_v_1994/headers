import "./BurgerComponent.scss";
import {BaseComponent} from "../BaseComponent.comp.js";

export class BurgerComponent extends BaseComponent {

    constructor() {
        super();
        this.initState = {
            body: "", // контент бургер меню
            open: false // открыто ли бургер меню
        };
        this.state = this.initState;
    }

    connectedCallback() {
        super.connectedCallback();
        this.querySelector('.burger-button').addEventListener('click', (event) => {
            this.querySelector('.burger').classList.toggle('burger-open')
        });
    }

    setBody(body) {
        this.state.body = body;
        this.querySelector('.burger-body').innerHTML = this.state.body;
    }

    render() {
        return `
            <div class="burger ${this.state.open ? 'burger-open' : ''}">
               <div class="burger-button">
                   <div></div>
               </div>
               <div class="burger-body"></div>
            </div>
        `;
    }
}

BurgerComponent.componentId = 'burger-component';

window.customElements.define(BurgerComponent.componentId, BurgerComponent);