import {BaseComponent} from "../BaseComponent.comp.js";
import "./ArkadiumMenu.scss";

export class ArkadiumMenuComponent extends BaseComponent {

    constructor() {
        super();
        this.initState = {
            menu: [
                {id: "", title: "all"},
                {id: "", title: "word"},
                {id: "", title: "card"},
                {id: "", title: "puzzles"},
                {id: "", title: "arkade"}
            ]
        };
        this.state = this.initState;
    }

    renderMenu(className) {
        return `
            <ul class="${className}">
                ${this.state.menu.map((_) => `<li class="${className}-item" data-id="${_.id}">${_.title}</li>`).join('')}
            </ul>
        `
    }

    render() {
        const i18n = this.getI18n();
        return `
            <div class="arkadium-menu">
                ${this.renderMenu("arkadium-menu-list")}
            </div>
        `;
    }
}

ArkadiumMenuComponent.componentId = 'arkadium-menu-component';

window.customElements.define(ArkadiumMenuComponent.componentId, ArkadiumMenuComponent);