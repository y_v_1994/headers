import {BaseComponent} from "../BaseComponent.comp.js";
import "./ClientMenu.scss";
import {RenderHelper} from "../../helpers/RenderHelper";

export class ClientMenuComponent extends BaseComponent {

    constructor() {
        super();
        this.initState = {menu: []};
        this.state = this.initState;
    }

    setMenu(menu) {
        this.state.menu = menu;
        this.connectedCallback();
    }

    render() {
        return `
            <div class="client-menu">
                ${RenderHelper.renderList("client-menu", this.state.menu)}
            </div>
        `;
    }
}

ClientMenuComponent.componentId = 'client-menu-component';

window.customElements.define(ClientMenuComponent.componentId, ClientMenuComponent);