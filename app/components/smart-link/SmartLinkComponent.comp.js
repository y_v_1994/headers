import {BaseComponent} from "../BaseComponent.comp.js";
import "./SmartLinkComponent.scss";

export class SmartLinkComponent extends BaseComponent {

    constructor() {
        super();
        this.initState = {title: '', href: '', icon: '', alt: ''};
        this.state = this.initState;
    }

    setData(data) {
        this.state = data;
        this.connectedCallback();
    }

    render() {
        return `
            <a class="smart-link-component">
                ${this.state.icon?`<img src="${this.state.icon}" alt="${this.state.alt}">`:''}
                ${this.state.iconSVG?`${this.state.iconSVG}`:''}
                ${this.state.title?`<span class="smart-link__title">${this.state.title}</span>`:''}
            </a>
        `;
    }
}

SmartLinkComponent.componentId = 'smart-link-component';

window.customElements.define(SmartLinkComponent.componentId, SmartLinkComponent);