import {BaseComponent} from "../BaseComponent.comp.js";
import "./SearchComponent.scss";
import searchImg from "./assets/search.svg";

export class SearchComponent extends BaseComponent {
    render() {
        return `
            <div class="search">
                <input class="search-input" type="text" placeholder="search">
                <button class="search-button">${searchImg}</button>
            </div>
        `;
    }
}

SearchComponent.componentId = 'search-component';
window.customElements.define(SearchComponent.componentId, SearchComponent);