import {BaseComponent} from "../BaseComponent.comp.js";
import "./LogoComponent.scss";

export class LogoComponent extends BaseComponent {
    render() {
        return `
            <div class="logo">
                Logo
            </div>
        `;
    }
}

LogoComponent.componentId = 'logo-component';

window.customElements.define(LogoComponent.componentId, LogoComponent);