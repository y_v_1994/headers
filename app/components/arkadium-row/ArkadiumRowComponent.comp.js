import {BaseComponent} from "../BaseComponent.comp.js";
import "./ArkadiumRowComponent.scss";

export class ArkadiumRowComponent extends BaseComponent {

    constructor() {
        super();
        this.initState = {
            menu: [
                {id: "", title: "all game"},
                {id: "", title: "all game"}
            ]
        };
        this.state = this.initState;
    }

    renderMenu() {
        return `
            <ul>
                ${this.state.menu.map((_) => `<li data-id="${_.id}">${_.title}</li>`).join('')}
            </ul>
        `
    }

    render() {
        const i18n = this.getI18n();
        return `
            <div class="arkadium-menu">
                ${this.renderMenu()}
            </div>
        `;
    }
}

ArkadiumRowComponent.componentId = 'arkadium-row-component';

window.customElements.define(ArkadiumRowComponent.componentId, ArkadiumRowComponent);