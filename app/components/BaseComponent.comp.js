import EventBus from "eventbusjs";
import {I18nService} from "../services/i18n/i18nService.service";

export class BaseComponent extends HTMLElement {

    constructor() {
        super();
        // this.getEventBus().addEventListener("RERENDER_EVENT", (event, args) => {
        //     this.connectedCallback();
        // });
    }

    static get observedAttributes() {
        return [];
    }

    attributeChangedCallback(name, oldValue, newValue) {
    }

    disconnectedCallback() {
    }

    connectedCallback() {
        this.innerHTML = this.render();
    }

    getEventBus() {
        return EventBus;
    }

    getLang() {
        return (new I18nService()).getState().lang;
    }

    renderComponent(componentClassName, id=null, className=null) {
        return `<${componentClassName.componentId} ${id?`id="${id}"`:''} ${className?`class="${className}"`:''}></${componentClassName.componentId}>`;
    }

    getI18n() {
        return (new I18nService()).getState().langPack;
    }

    render() {
        return ``;
    }

}