import {BaseComponent} from "../BaseComponent.comp.js";
import "./AdaptiveNavigationComponent.scss";
import {UtilHelper} from "../../helpers/UtilHelper";

export class AdaptiveNavigationComponent extends BaseComponent {

    constructor() {
        super();
        this.initState = {
            menu: [
                {id: "", title: "all"},
                {id: "", title: "word"},
                {id: "", title: "card"},
                {id: "", title: "puzzles"},
                {id: "", title: "arkade"},
                {id: "", title: "all"},
                {id: "", title: "word"},
                {id: "", title: "card"},
                {id: "", title: "puzzles"},
                {id: "", title: "arkade"},
                {id: "", title: "all"},
                {id: "", title: "word"},
                {id: "", title: "card"},
                {id: "", title: "puzzles"},
                {id: "", title: "arkade"},
                {id: "", title: "all"},
                {id: "", title: "word"},
                {id: "", title: "card"},
                {id: "", title: "puzzles"},
                {id: "", title: "arkade"},
                {id: "", title: "all"},
                {id: "", title: "word"},
                {id: "", title: "card"},
                {id: "", title: "puzzles"},
                {id: "", title: "arkade"}
            ],
            navWidth: 0,
            showedItems: [],
            hiddenItems: [],
            hiddenItemsOpened: false
        };
        this.state = this.initState;
    }

    connectedCallback() {
        this.state.showedItems = this.state.menu;
        this.state.hiddenItems = [];
        super.connectedCallback();
        window.addEventListener('resize', UtilHelper.debounce(this.onResize.bind(this), 59));
        this.querySelector('.reset').addEventListener('click', this.onMoreButtonClick.bind(this));

        setTimeout(() => {
            this.onResize();
        }, 200);
    }

    setNav(nav) {
        this.state.menu = [];
        this.state.menu.push(...nav);
        this.state.menu.reverse();
        this.state.showedItems = this.state.menu;
        this.state.hiddenItems = [];
        const navList = this.querySelector('.adaptive-navigation-list');

        let maxWidth = 0;

        this.querySelectorAll(".adaptive-navigation-list .adaptive-navigation-list__item").forEach((_) => {_.remove()});
        this.state.showedItems.reverse().forEach((_) => {
            let li = document.createElement("li");
            li.setAttribute("data-id", _.id);
            li.classList.add("adaptive-navigation-list__item");
            li.innerHTML = _.title;
            navList.prepend(li);
            maxWidth += li.clientWidth;
        });

        const adaptiveNavigation = this.querySelector('.adaptive-navigation');
        const buttonWidth = this.querySelector('.adaptive-navigation-list__item--more').clientWidth;
        const minWidth = this.querySelectorAll(".adaptive-navigation-list > .adaptive-navigation-list__item")[0].clientWidth;
        adaptiveNavigation.style.minWidth = `${minWidth + buttonWidth}px`;
        this.style.maxWidth = `${maxWidth + buttonWidth + 20}px`;

        this.onResize();
    }

    onResize() {
        const nav = this.querySelector(".adaptive-navigation");
        this.state.navWidth = nav.clientWidth;
        const notHiddenItems = this.getElementsNotHidden();
        this.state.showedItems = this.state.menu.slice(0, notHiddenItems.length);
        this.state.hiddenItems = this.state.menu.slice(notHiddenItems.length);
        this.renderMenuOnResize();
    }

    onMoreButtonClick() {
        this.state.hiddenItemsOpened = !this.state.hiddenItemsOpened;
        this.querySelector('.adaptive-navigation-list__item--more').classList.toggle('adaptive-navigation-list__item--opened');
    }

    // найти элементs меню которые поместятся в контейнер
    getElementsNotHidden() {
        let items = this.querySelectorAll(".adaptive-navigation-list__item");
        const resetButtonWidth = 36;
        let navWidth = this.state.navWidth - resetButtonWidth;
        let itemsWidth = 0;
        let result = [];
        for (let i = 0; i < items.length; i++) {
            itemsWidth += items[i].clientWidth;
            if (itemsWidth >= navWidth) {
                return result;
            }
            result.push(items[i]);
        }
        return result;
    }

    renderMenuOnResize() {
        const nav = this.querySelector('.adaptive-navigation-list');
        const hiddenNav = this.querySelector('.adaptive-navigation-list__item--more #adapt-nav-dropdown');
        this.querySelectorAll(".adaptive-navigation-list .adaptive-navigation-list__item").forEach((_) => {_.remove()});

        let maxWidth = 0;
        const buttonWidth = this.querySelector('.adaptive-navigation-list__item--more').clientWidth;


        this.state.showedItems.reverse().forEach((_) => {
            let li = document.createElement("li");
            li.setAttribute("data-id", _.id);
            li.classList.add("adaptive-navigation-list__item");
            li.innerHTML = _.title;
            nav.prepend(li);
            maxWidth += li.clientWidth;
        });




        this.state.hiddenItems.reverse().forEach((_) => {
            let li = document.createElement("li");
            li.setAttribute("data-id", _.id);
            li.classList.add("adaptive-navigation-list__item");
            li.innerHTML = _.title;
            hiddenNav.prepend(li);
        });
        const adaptiveNavigation = this.querySelector('.adaptive-navigation');
        if (!this.state.hiddenItems.length) {
            adaptiveNavigation.classList.add('adaptive-navigation--hidden-button');
        } else {
            adaptiveNavigation.classList.remove('adaptive-navigation--hidden-button');
        }


        // this.style.maxWidth = `${maxWidth + buttonWidth}px`;
    }

    renderMenu(className) {
        return `
            <ul class="${className}">
                ${this.state.menu.map((_) => `<li class="${className}__item" data-id="${_.id}">${_.title}</li>`).join('')}
                <li class="${className}__item--more">
                    <button class="reset" data-label="Show more items">
                        <svg class="icon" viewBox="0 0 16 16">
                            <circle cx="8" cy="7.5" r="1.5"></circle>
                            <circle cx="1.5" cy="7.5" r="1.5"></circle>
                            <circle cx="14.5" cy="7.5" r="1.5"></circle>
                        </svg>
                    </button>
                    <ul id="adapt-nav-dropdown"></ul>
                </li>
            </ul>
        `
    }

    render() {
        const i18n = this.getI18n();
        return `
            <div class="">
                <nav class="adaptive-navigation">
                    ${this.renderMenu("adaptive-navigation-list")}
                </nav>
            </div>
        `;
    }
}

AdaptiveNavigationComponent.componentId = 'adaptive-navigation-component';

window.customElements.define(AdaptiveNavigationComponent.componentId, AdaptiveNavigationComponent);











