import {BaseComponent} from "../BaseComponent.comp";
import {DropDownComponent} from "../drop-down/DropDownComponent.comp";

export class LangSelectorComponent extends BaseComponent {

    constructor() {
        super();
        this.initState = {buttonContent: '', bodyContent: ''};
        this.state = this.initState;
    }

    connectedCallback() {
        super.connectedCallback();
        this.querySelector(DropDownComponent.componentId).setButton(`<div>English</div>`);
        this.querySelector(DropDownComponent.componentId).setBody(`
            <ul>
                <li id="en" class="lang-button" data-lang="en">English</li>
                <li id="fr" class="lang-button" data-lang="fr">Français</li>
                <li id="de" class="lang-button" data-lang="de">Deutsch</li>
                <li id="de" class="lang-button" data-lang="de">Italiano</li>
                <li id="de" class="lang-button" data-lang="de">Español</li>
                <li id="de" class="lang-button" data-lang="de">Português</li>
                <li id="de" class="lang-button" data-lang="de">Русский</li>
                <li id="de" class="lang-button" data-lang="de">Türkçe</li>
                <li id="de" class="lang-button" data-lang="de">Ελληνικά</li>
                <li id="de" class="lang-button" data-lang="de">Magyar</li>
                <li id="de" class="lang-button" data-lang="de">فارسی</li>
                <li id="de" class="lang-button" data-lang="de">العربية</li>
            </ul>
        `);
        this.querySelectorAll('.lang-button').forEach((_) => {
            _.addEventListener('click', (event) => {
                const lang = event.target.getAttribute('data-lang');
                this.getEventBus().dispatch("CHANGE_LANG_EVENT", this, {lang: lang, langPack: {LOGIN: "login", GAME: "game"}});
            });
        });
    }

    setButton(button) {
        this.querySelector(DropDownComponent.componentId).setButton(button);
    }



    render() {
        return `
            <div class="lang-selector-component">
                ${this.renderComponent(DropDownComponent)}
            </div>
        `;
    }
}

LangSelectorComponent.componentId = 'lang-selector-component';

window.customElements.define(LangSelectorComponent.componentId, LangSelectorComponent);