import {BaseComponent} from "../BaseComponent.comp.js";
import "./LoginButton.scss";
import userImg from "./assets/user.svg";
import dropDownArrowImg from "./assets/drop-down-arrow.svg";

export class LoginButtonComponent extends BaseComponent {

    constructor() {
        super();
        this.initState = {logined: false, loginedPanelOpen: false, userName: "userName"};
        this.state = this.initState;
    }

    connectedCallback() {
        super.connectedCallback();
        this.getEventBus().addEventListener("CHANGE_LOGIN_STATE_EVENT", (event, args) => {
            if (args.logined) {
                this.state = args;
            } else {
                this.state = this.initState;
            }
            this.connectedCallback();
        });
        this.querySelector('.login-button').addEventListener('click', (event) => {
            this.state.logined = true;
            this.querySelector('.login').classList.add('logined');
        });
        this.querySelector('.user-info-button').addEventListener('click', (event) => {
            this.state.loginedPanelOpen = !this.state.loginedPanelOpen;
            this.querySelector('.logined-state').classList.toggle('logined-state-open');
        });
        this.querySelector('.logout-button').addEventListener('click', (event) => {
            this.state.logined = false;
            this.querySelector('.login').classList.remove('logined');
        });
    }

    renderLoginedState() {
        return `
            <div class="logined-state">
                <button class="button user-info-button">${userImg} ${this.state.userName} ${dropDownArrowImg}</button>
                <div class="logined-state__panel">
                    <button>profile</button>
                    <button class="logout-button">logout</button>
                </div>
            </div>
           `;
    }

    render() {
        const i18n = this.getI18n();
        return `
            <div class="login">
                <button class="button login-button">${i18n.LOGIN}</button>
                ${this.renderLoginedState()}
            </div>
        `;
    }
}

LoginButtonComponent.componentId = 'login-button-component';

window.customElements.define(LoginButtonComponent.componentId, LoginButtonComponent);