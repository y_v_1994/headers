import {BaseComponent} from "../BaseComponent.comp.js";
import "./DropDownComponent.scss";

export class DropDownComponent extends BaseComponent {

    constructor() {
        super();
        this.initState = {buttonContent: '', bodyContent: ''};
        this.state = this.initState;
    }

    connectedCallback() {
        super.connectedCallback()
        this.querySelector('.drop-down__button').addEventListener('click', () => {
            this.querySelector('.drop-down-component').classList.toggle('drop-down-component--opened');
        });
    }

    setButton(buttonContent) {
        this.state.buttonContent = buttonContent;
        this.querySelector('.drop-down__button').innerHTML = this.state.buttonContent;
    }

    setBody(bodyContent) {
        this.state.bodyContent = bodyContent;
        this.querySelector('.drop-down__body').innerHTML = this.state.bodyContent;
    }

    render() {
        return `
            <div class="drop-down-component">
                <div class="drop-down__button"></div>
                <div class="drop-down__body"></div>
            </div>
        `;
    }
}

DropDownComponent.componentId = 'drop-down-component';

window.customElements.define(DropDownComponent.componentId, DropDownComponent);