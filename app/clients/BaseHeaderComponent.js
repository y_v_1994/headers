import "@webcomponents/webcomponentsjs/webcomponents-lite";
import {BaseComponent} from "../components/BaseComponent.comp";
import "./BaseHeaderComponent.scss";
import {UtilHelper} from "../helpers/UtilHelper";

export const TABLET_BACKPOINT = 768;
export const DESKTOP_BACKPOINT = 1024;

export const DEVICE_PC = 'pc';
export const DEVICE_TABLET = 'tablet';
export const DEVICE_PHONE = 'phone';

export class BaseHeaderComponent extends BaseComponent {

    constructor() {
        super();
        this.initState = {device: null};
        this.state = this.initState;
        this.getEventBus().addEventListener("CHANGE_LANG_EVENT", (event, args) => {
            if (this.state.device === DEVICE_PC) {
                setTimeout(() => {this.initComponent();}, 200);
            } else if (this.state.device === DEVICE_TABLET) {
                setTimeout(() => {this.initTabletComponent();}, 200);
            } else if (this.state.device === DEVICE_PHONE) {
                setTimeout(() => {this.initPhoneComponent();}, 200);
            }
        });
    }

    connectedCallback() {
        window.addEventListener('resize', UtilHelper.debounce(this.onResize.bind(this), 100));
        this.onResize();
    }

    initComponent() {}

    /* ============= */
    /* RENDER TABLET */
    /* ============= */
    initTabletComponent() { return this.initComponent();}
    renderTablet() { return this.render();}

    /* ============ */
    /* RENDER PHONE */
    /* ============ */
    initPhoneComponent() { return this.initComponent();}
    renderPhone() { return this.render();}

    onResize() {
        const windowWidth = window.innerWidth;
        if (windowWidth > 1024) {
            if (this.state.device !== DEVICE_PC) {
                this.innerHTML = this.render();
                this.initComponent();
                this.state.device = DEVICE_PC;
            }
        } else if (windowWidth > 768) {
            if (this.state.device !== DEVICE_TABLET) {
                this.innerHTML = this.renderTablet();
                this.initTabletComponent();
                this.state.device = DEVICE_TABLET;
            }
        } else {
            if (this.state.device !== DEVICE_PHONE) {
                this.innerHTML = this.renderPhone();
                this.initPhoneComponent();
                this.state.device = DEVICE_PHONE;
            }
        }
    }

}