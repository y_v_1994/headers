import "./euronews.scss";
import {SearchComponent} from "../../components/search/SearchComponent.comp";
import {ArkadiumMenuComponent} from "../../components/arkadium-menu/ArkadiumMenuComponent.comp";
import {LoginButtonComponent} from "../../components/login-button/LoginButtonComponent.comp";
import {BurgerComponent} from "../../components/burger/BurgerComponent.comp";
import {BaseHeaderComponent} from "../BaseHeaderComponent";
import {SmartLinkComponent} from "../../components/smart-link/SmartLinkComponent.comp";
import {CLIENT_MENU, CLIENT_MENU_PROGRAMMES, BULLETIN_LINK, LIVE_TV_LINK} from "./client-menu";
import {DropDownComponent} from "../../components/drop-down/DropDownComponent.comp";
import logoImg from "./assets/logo.svg";
import flightImg from "./assets/flight.svg";
import newsletterImg from "./assets/newsletter.svg";
import gamesImg from "./assets/games.svg";
import {LangSelectorComponent} from "../../components/lang-selector/LangSelectorComponent.comp";
import programsImg from "./assets/programs.svg";
import justInImg from "./assets/just-in.svg";
import bulletinImg from "./assets/bulletin.svg";
import liveTvImg from "./assets/live-tv.svg";
import {AdaptiveNavigationComponent} from "../../components/adaptive-navigation/AdaptiveNavigationComponent.comp";
import {RenderHelper} from "../../helpers/RenderHelper";
import languageImg from "./assets/language.svg";

export class EuronewsHeaderComponent extends BaseHeaderComponent {

    renderProgrammes(list) {
        return `${RenderHelper.renderListRecursive('euronews-drop-down', list)}`;
    }

    /* ====== */
    /* RENDER */
    /* ====== */
    initComponent() {
        this.querySelector('#newsletter-link').setData({title: 'Newsletter', href: '', iconSVG: newsletterImg});
        this.querySelector('#flight-link').setData({href: '', iconSVG: flightImg});
        this.querySelector('#games-link').setData({href: '', iconSVG: gamesImg});
        this.querySelector('#drop-down-navigation').setButton(`<div class="programmes-button">${programsImg} Programmes</div>`);
        this.querySelector('#drop-down-navigation').setBody(`${this.renderProgrammes(CLIENT_MENU_PROGRAMMES[this.getLang()])}`);
        this.querySelector('#just-in-link').setData({href: '', iconSVG: justInImg});
        this.querySelector('#bullet-in-link').setData({href: '', iconSVG: bulletinImg});
        this.querySelector('#live-tv-link').setData({href: '', title: 'Live TV', iconSVG: liveTvImg});
        this.querySelector(AdaptiveNavigationComponent.componentId).setNav(CLIENT_MENU[this.getLang()]);
        this.querySelector(LangSelectorComponent.componentId).setButton(`${languageImg} English`);
    }
    render() {
        return `
            <div class="header euronews-header">
                <div class="row row-lang">
                    <div>
                        ${this.renderComponent(LangSelectorComponent)}        
                    </div>
                    <div class="euronews-top-links">
                        ${this.renderComponent(SmartLinkComponent, 'newsletter-link', 'euronews-top-link')}
                        ${this.renderComponent(SmartLinkComponent, 'flight-link', 'euronews-top-link')}
                        ${this.renderComponent(SmartLinkComponent, 'games-link', 'euronews-top-link')}
                    </div>
                </div>
                <div class="row row-logo">
                    ${logoImg}
                </div>
                <div class="row row-client-menu">
                    ${this.renderComponent(AdaptiveNavigationComponent)}
                    ${this.renderComponent(DropDownComponent, 'drop-down-navigation')}
                    ${this.renderComponent(SmartLinkComponent, 'just-in-link', 'euronews-link')}
                    ${this.renderComponent(SmartLinkComponent, 'bullet-in-link', 'euronews-link')}
                    ${this.renderComponent(SmartLinkComponent, 'live-tv-link', 'euronews-link')}
                </div>
                <div class="row row-arkadium">
                    <div class="row">
                        ${this.renderComponent(ArkadiumMenuComponent)}
                        ${this.renderComponent(SearchComponent)}
                    </div>
                    ${this.renderComponent(LoginButtonComponent)}
                </div>
            </div>
        `;
    }

    /* ============= */
    /* RENDER TABLET */
    /* ============= */
    initTabletComponent() {
        this.querySelector('#drop-down-navigation').setButton(`<div class="programmes-button">${programsImg} Programmes</div>`);
        this.querySelector('#drop-down-navigation').setBody(`${this.renderProgrammes(CLIENT_MENU_PROGRAMMES[this.getLang()])}`);
        this.querySelector('#plus-navigation').setButton(`<div class="programmes-button">Plus</div>`);
        this.querySelector('#plus-navigation').setBody(`${this.renderProgrammes(CLIENT_MENU[this.getLang()])}`);
        this.querySelector('#just-in-link').setData({href: '', iconSVG: justInImg});
        this.querySelector('#bullet-in-link').setData({href: '', iconSVG: bulletinImg});
        this.querySelector('#live-tv-link').setData({href: '', title: 'Live TV', iconSVG: liveTvImg});
        this.initBurgerContent();
    }
    renderTablet() {
        return `
            <div class="header euronews-header header-tablet">
                <div class="row row-burger space-between">
                    ${this.renderComponent(BurgerComponent)}
                    ${this.renderComponent(LangSelectorComponent)}
                </div>
                <div class="row row-logo">
                    ${logoImg}
                </div>
                <div class="row row-client-menu">
                    ${this.renderComponent(DropDownComponent, 'plus-navigation')}
                    ${this.renderComponent(DropDownComponent, 'drop-down-navigation')}
                    ${this.renderComponent(SmartLinkComponent, 'just-in-link', 'euronews-link just-in-link')}
                    ${this.renderComponent(SmartLinkComponent, 'bullet-in-link', 'euronews-link bullet-in-link')}
                    ${this.renderComponent(SmartLinkComponent, 'live-tv-link', 'euronews-link live-tv-link')}
                </div>
            </div>
           `;
    }

    /* ============ */
    /* RENDER PHONE */
    /* ============ */
    initPhoneComponent() {
        this.querySelector('#just-in-link').setData({href: '', iconSVG: justInImg});
        this.querySelector('#bullet-in-link').setData({href: '', iconSVG: bulletinImg});
        this.initBurgerContent();
    }
    renderPhone() {
        return `
            <div class="header euronews-header header-phone">
                <div class="row row-burger">
                    <div class="row">
                        ${this.renderComponent(BurgerComponent)}
                        ${logoImg}
                    </div>
                    <div class="row">
                        ${this.renderComponent(SmartLinkComponent, 'just-in-link', 'euronews-link')}
                        ${this.renderComponent(SmartLinkComponent, 'bullet-in-link', 'euronews-link')}
                    </div>
                </div>
            </div>
           `;
    }

    /* ============= */
    /* RENDER BURGER */
    /* ============= */
    initBurgerContent() {
        this.querySelector(BurgerComponent.componentId).setBody(this.renderBurgerContent());
        this.querySelector('#newsletter-burger-link').setData({title: 'Newsletter', href: '', iconSVG: newsletterImg});
        this.querySelector('#flight-burger-link').setData({href: '', title: 'Book your flight', iconSVG: flightImg});
        this.querySelector('#games-burger-link').setData({href: '', title: 'Games', iconSVG: gamesImg});
        this.querySelector('#just-in-burger-link').setData({href: '', title: 'Just In', iconSVG: justInImg});
        this.querySelector('#bullet-in-burger-link').setData({href: '', title: 'Live', iconSVG: bulletinImg});
        this.querySelector('#live-tv-burger-link').setData({href: '', title: LIVE_TV_LINK[this.getLang()].title, iconSVG: liveTvImg});
        this.querySelector(LangSelectorComponent.componentId).setButton(`${languageImg} English`);
    }

    renderBurgerContent() {
        return `
            ${this.renderProgrammes(CLIENT_MENU[this.getLang()])}
            <div class="burger-body-links">
                ${this.renderComponent(SmartLinkComponent, 'just-in-burger-link', 'euronews-link euronews-burger-link')}
                ${this.renderComponent(SmartLinkComponent, 'bullet-in-burger-link', 'euronews-link euronews-burger-link')}
                ${this.renderComponent(SmartLinkComponent, 'live-tv-burger-link', 'euronews-link euronews-burger-link')}
                ${this.renderComponent(SmartLinkComponent, 'newsletter-burger-link', 'euronews-link euronews-burger-link')}
                ${this.renderComponent(SmartLinkComponent, 'flight-burger-link', 'euronews-link euronews-burger-link')}
                ${this.renderComponent(SmartLinkComponent, 'games-burger-link', 'euronews-link euronews-burger-link')}
            </div>
            ${this.renderComponent(LangSelectorComponent, 'lang-selector-burger')}
        `;
    }

}

window.customElements.define('euronews-header-component', EuronewsHeaderComponent);