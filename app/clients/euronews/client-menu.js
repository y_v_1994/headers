export const CLIENT_MENU = {
  en: [
      {
          title:"Europe",
          href:"http://www.euronews.com/european-affairs/european-news",
      },
      {
          title:"World",
          href:"http://www.euronews.com/news/international",
      },
      {
          title:"Business",
          href:"http://www.euronews.com/news/business",
      },
      {
          title:"Sport",
          href:"http://www.euronews.com/news/sport",
      },
      {
          title:"Culture",
          href:"http://www.euronews.com/lifestyle/culture",
      },
      {
          title:"Style",
          href:"http://www.livingit.euronews.com/",
      },
      {
          title:"Sci-tech",
          href:"http://www.euronews.com/knowledge/sci-tech",
      },
      {
          title:"Travel",
          href:"http://www.euronews.com/lifestyle/travel",
      },
      {
          title:"Video",
          href:"http://www.euronews.com/video",
      }
  ],
  de: [
      {
          title:"Europa",
          "url": "http://de.euronews.com/european-affairs/nachrichten-aus-europa",
      },
      {
          "title": "Welt",
          "url": "http://de.euronews.com/nachrichten/international",
      },
      {
          "title": "Wirtschaft",
          "url": "http://de.euronews.com/nachrichten/business",
      },
      {
          "title": "Sport",
          "url": "http://de.euronews.com/nachrichten/sport",
      },
      {
          "title": "Kultur",
          "url": "http://de.euronews.com/lifestyle/kultur",
      },
      {
          "title": "Sci-Tech",
          "url": "http://de.euronews.com/knowledge/sci-tech",
      },
      {
          "title": "Reise",
          "url": "http://de.euronews.com/lifestyle/reise",
      },
      {
          "title": "Video",
          "url": "http://de.euronews.com/video",
      }
  ],
  fr: [
      {
          "title":"Europe",
          "url":"http://fr.euronews.com/european-affairs/infos-europeenes",
      },
      {
          "title":"Monde",
          "url":"http://fr.euronews.com/infos/internationale",
      },
      {
          "title":"Business",
          "url":"http://fr.euronews.com/infos/business",
      },
      {
          "title":"Sport",
          "url":"http://fr.euronews.com/infos/sport",
      },
      {
          "title":"Culture",
          "url":"http://fr.euronews.com/lifestyle/culture",
      },
      {
          "title":"Sci-tech",
          "url":"http://fr.euronews.com/knowledge/sci-tech",
      },
      {
          "title":"Voyage",
          "url":"http://fr.euronews.com/lifestyle/voyages",
      },
      {
          "title":"Vidéo",
          "url":"http://fr.euronews.com/video",
      }
  ]
};

export const CLIENT_MENU_PROGRAMMES = {
  en: [
      {
          title:"Europe",
          href:"http://www.euronews.com/european-affairs/european-news",
          items: [
              {
                  title:"Brussels Bureau",
                  href:"http://www.euronews.com/programs/brussels-bureau"
              },
              {
                  title:"State Of The Union",
                  href:"http://www.euronews.com/programs/state-of-the-union"
              },
              {
                  title:"Smart Regions",
                  href:"http://www.euronews.com/programs/smart-regions"
              },
          ]
      },
      {
          title:"World",
          href:"http://www.euronews.com/news/international",
          items: [
              {
                  title:"World News",
                  href:"http://www.euronews.com/programs/world"
              },
              {
                  title:"Insiders",
                  href:"http://www.euronews.com/programs/insiders"
              },
              {
                  title:"Insight",
                  href:"http://www.euronews.com/programs/insight"
              },
              {
                  title:"Global Japan",
                  href:"http://www.euronews.com/programs/global-japan"
              },
              {
                  title:"Inspire Middle East",
                  href:"http://www.euronews.com/programs/inspire-middle-east"
              },
              {
                  title:"The Global Conversation",
                  href:"http://www.euronews.com/programs/globalconversation"
              },
              {
                  title: "Raw Politics",
                  href: "http://www.euronews.com//programs/raw-politics"
              },
              {
                  title:"View",
                  href:"http://www.euronews.com/programs/view"
              },
              {
                  title:"Aid Zone",
                  href:"http://www.euronews.com/programs/aid-zone"
              },
              {
                  title:"Good Morning Europe",
                  href:"http://www.euronews.com/programs/good-morning-europe"
              },

          ]
      },
      {
          title:"Business",
          href:"http://www.euronews.com/news/business",
          items: [
              {
                  title:"Business Planet",
                  href:"http://www.euronews.com/programs/business-planet"
              },
              {
                  title:"Real Economy",
                  href:"http://www.euronews.com/programs/realeconomy"
              },
              {
                  title:"Target",
                  href:"http://www.euronews.com/programs/target"
              },
          ]
      },
      {
          title:"Culture",
          href:"http://www.euronews.com/lifestyle/culture",
          items: [
              {
                  title:"Cinema",
                  href:"http://www.euronews.com/programs/cinema"
              },
              {
                  title:"Cult",
                  href:"http://www.euronews.com/programs/cult"
              },
              {
                  title:"Musica",
                  href:"http://www.euronews.com/programs/musica"
              },
              {
                  title:"European Lens",
                  href:"http://www.euronews.com/programs/europeanlens"
              },
              {
                  title:"Adventures",
                  href:"http://www.euronews.com/programs/adventures"
              },
              {
                  title:"Style",
                  href:"//www.livingit.euronews.com/style"
              },
          ]
      },
      {
          title:"Sci-tech",
          href:"/knowledge/sci-tech",
          items: [
              {
                  title:"Futuris",
                  href:"http://www.euronews.com/programs/futuris"
              },
              {
                  title:"Sci-Tech",
                  href:"http://www.euronews.com/programs/sci-tech"
              },
              {
                  title:"Farnborough Airshow",
                  href:"http://www.euronews.com/programs/farnborough-airshow"
              },
              {
                  title:"Space",
                  href:"http://www.euronews.com/programs/space"
              },
              {
                  title:"Rides & Experiences",
                  href:"//www.livingit.euronews.com/rides-and-experiences"
              },
          ]
      },
      {
          title:"Travel",
          href:"http://www.euronews.com/lifestyle/travel",
          items: [
              {
                  title:"Focus",
                  href:"http://www.euronews.com/programs/focus"
              },
              {
                  title:"Postcards",
                  href:"http://www.euronews.com/programs/postcards"
              },
              {
                  title:"Wander",
                  href:"http://www.euronews.com/programs/wander"
              },
              {
                  title:"Metropolitans",
                  href:"http://www.euronews.com/programs/metropolitans"
              },
              {
                  title:"Taste",
                  href:"http://www.euronews.com/programs/taste"
              },
          ]
      }
  ],
  de: [
      {
          "title": "Europa",
          "url": "http://de.euronews.com/european-affairs/nachrichten-aus-europa",
          items: [
              {
                  "title": "Redaktion Brüssel",
                  "url": "http://de.euronews.com/programme/redaktion-brussel"
              },
              {
                  "title": "State Of The Union",
                  "url": "http://de.euronews.com/programme/state-of-the-union"
              },
              {
                  "title": "Smart Regions",
                  "url": "http://de.euronews.com/programme/smart-regions"
              },
          ]
      },
      {
          "title": "Welt",
          "url": "http://de.euronews.com/nachrichten/international",
          items: [
              {
                  "title": "Welt",
                  "url": "http://de.euronews.com/programme/welt"
              },
              {
                  "title": "Insiders",
                  "url": "http://de.euronews.com/programme/insiders"
              },
              {
                  "title": "Insight",
                  "url": "http://de.euronews.com/programme/insight"
              },
              {
                  "title": "Global Japan",
                  "url": "http://de.euronews.com/programme/global-japan"
              },
              {
                  "title": "The Global Conversation",
                  "url": "http://de.euronews.com/programme/globalconversation"
              },
              {
                  "title": "View",
                  "url": "http://de.euronews.com/programme/view"
              },
              {
                  "title": "Aid Zone",
                  "url": "http://de.euronews.com/programme/aid-zone"
              },
          ]
      },
      {
          "title": "Wirtschaft",
          "url": "http://de.euronews.com/nachrichten/business",
          items: [
              {
                  "title": "Business Planet",
                  "url": "http://de.euronews.com/programme/business-planet"
              },
              {
                  "title": "Real Economy",
                  "url": "http://de.euronews.com//programme/realeconomy"
              },
              {
                  "title": "Target",
                  "url": "http://de.euronews.com/programme/target"
              },
          ]
      },
      {
          "title": "Kultur",
          "url": "http://de.euronews.com/lifestyle/kultur",
          items: [
              {
                  "title": "Cinema",
                  "url": "http://de.euronews.com/programme/cinema"
              },
              {
                  "title": "Cult",
                  "url": "http://de.euronews.com/programme/cult"
              },
              {
                  "title": "Musica",
                  "url": "http://de.euronews.com/programme/musica"
              },
              {
                  "title": "European Lens",
                  "url": "http://de.euronews.com/programme/europeanlens"
              },
              {
                  "title": "Adventures",
                  "url": "http://de.euronews.com/programme/adventures"
              },
          ]
      },
      {
          "title": "Sci-Tech",
          "url": "http://de.euronews.com/knowledge/sci-tech",
          items: [
              {
                  "title": "Futuris",
                  "url": "http://de.euronews.com/programme/futuris"
              },
              {
                  "title": "Sci-Tech",
                  "url": "http://de.euronews.com/programme/sci-tech"
              },

              {
                  "title": "Space",
                  "url": "http://de.euronews.com/programme/space"
              }
          ]
      },
      {
          "title": "Reise",
          "url": "http://de.euronews.com/lifestyle/reise",
          items: [
              {
                  title:"Focus",
                  href:"http://www.euronews.com/programs/focus"
              },
              {
                  title:"Postcards",
                  href:"http://www.euronews.com/programs/postcards"
              },
              {
                  title:"Metropolitans",
                  href:"http://www.euronews.com/programs/metropolitans"
              },
              {
                  title:"Taste",
                  href:"http://www.euronews.com/programs/taste"
              },
          ]
      }
  ]
};

export const BULLETIN_LINK = {
    en: {
        title: 'Bulletin', href: 'http://www.euronews.com/bulletin'
    },
    de: {
        title: 'Bulletin', href: 'http://de.euronews.com/bulletin'
    }
};

export const LIVE_TV_LINK = {
    en: {
        title: 'Live TV', href: 'http://www.euronews.com/bulletin'
    },
    de: {
        title: 'DEDEDE', href: 'http://de.euronews.com/bulletin'
    }
}