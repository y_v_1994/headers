export const CLIENT_MENU = [
    {
        title: "News",
        href: "https://www.dallasnews.com/news"
    },
    {
        title: "Business",
        href: "https://www.dallasnews.com/business"
    },
    {
        title: "Sports",
        href: "https://www.dallasnews.com/sports"
    },
    {
        title: "High School Sports",
        href: "https://www.dallasnews.com/high-school-sports"
    },
    {
        title: "Arts & Entertainment ",
        href: "https://www.dallasnews.com/arts-entertainment"
    },
    {
        title: "Food",
        href: "https://www.dallasnews.com/food"
    },
    {
        title: "Things to Do",
        href: "https://www.dallasnews.com/arts-entertainment/things-to-do"
    },
    {
        title: "Opinion",
        href: "https://www.dallasnews.com/opinion"
    }
];