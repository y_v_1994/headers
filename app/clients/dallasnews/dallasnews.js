import "./dallasnews.scss";
import {SearchComponent} from "../../components/search/SearchComponent.comp";
import {ArkadiumMenuComponent} from "../../components/arkadium-menu/ArkadiumMenuComponent.comp";
import {LoginButtonComponent} from "../../components/login-button/LoginButtonComponent.comp";
import {LogoComponent} from "../../components/logo/LogoComponent.comp";
import {ClientMenuComponent} from "../../components/client-menu/ClientMenuComponent.comp";
import {BurgerComponent} from "../../components/burger/BurgerComponent.comp";
import {BaseHeaderComponent} from "../BaseHeaderComponent";
import {CLIENT_MENU} from "./client-menu";
import logoImg from "./assets/logo.svg";

import {AdaptiveNavigationComponent} from "../../components/adaptive-navigation/AdaptiveNavigationComponent.comp";

export class DallasnewsHeaderComponent extends BaseHeaderComponent {

    connectedCallback() {
        super.connectedCallback();
    }

    renderBurderBody() {
        return `
            <div>
                ${this.renderComponent(LogoComponent)}
            </div>`;
    }

    initComponent() {
        this.querySelector(ClientMenuComponent.componentId).setMenu(CLIENT_MENU);
        this.querySelector(BurgerComponent.componentId).setBody(this.renderBurderBody());
    }
    render() {
        return `
            <div class="header">
                <div class="row row-logo">
                    <div></div>
                    ${logoImg}
                    <div class="right">
                        <a class="subscribe-link" href="https://join.dallasnews.com/subscription-landing">Subscribe</a>
                    </div>
                </div>
                <div class="row row-client-menu">
                    ${this.renderComponent(ClientMenuComponent)}
                </div>
                <div class="row row-arkadium">
                    <div class="row">
                        ${this.renderComponent(ArkadiumMenuComponent)}
                        ${this.renderComponent(SearchComponent)}
                    </div>
                    ${this.renderComponent(LoginButtonComponent)}
                </div>
            </div>
        `;
    }

    renderTablet() {
        return `
            <div class="header header-tablet">
                <div class="row row-logo">
                    ${logoImg}
                </div>
                <div class="row row-client-menu">
                    ${this.renderComponent(ClientMenuComponent)}
                </div>
                <div class="row row-arkadium">
                    <div class="row">
                        ${this.renderComponent(ArkadiumMenuComponent)}
                        ${this.renderComponent(SearchComponent)}
                    </div>
                    ${this.renderComponent(LoginButtonComponent)}
                </div>
            </div>
        `;
    }

}

window.customElements.define('dallasnews-header-component', DallasnewsHeaderComponent);