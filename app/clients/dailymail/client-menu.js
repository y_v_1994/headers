export const CLIENT_MENU = [
    {
        title: "Home",
        href: "http://www.dailymail.co.uk/home/index.html"
    },
    {
        title: "News",
        href: "http://www.dailymail.co.uk/news/index.html"
    },
    {
        title: "U.S.",
        href: "http://www.dailymail.co.uk/ushome/index.html"
    },
    {
        title: "Sport",
        href: "http://www.dailymail.co.uk/sport/index.html"
    },
    {
        title: "TV&Showbiz",
        href: "http://www.dailymail.co.uk/tvshowbiz/index.html"
    },
    {
        title: "Australia",
        href: "http://www.dailymail.co.uk/auhome/index.html"
    },
    {
        title: "Femail",
        href: "http://www.dailymail.co.uk/femail/index.html"
    },
    {
        title: "Health",
        href: "http://www.dailymail.co.uk/health/index.html"
    },
    {
        title: "Science",
        href: "http://www.dailymail.co.uk/sciencetech/index.html"
    },
    {
        title: "Money",
        href: "http://www.dailymail.co.uk/money/index.html"
    },
    {
        title: "Video",
        href: "http://www.dailymail.co.uk/video/index.html"
    },
    {
        title: "Travel",
        href: "http://www.dailymail.co.uk/travel/index.html"
    },
    {
        title: "Fashion Finder",
        href: "http://www.dailymail.co.uk/femail/fashionfinder/index.html"
    }
];