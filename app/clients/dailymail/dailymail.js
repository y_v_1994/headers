import "./dailymail.scss";
import {SearchComponent} from "../../components/search/SearchComponent.comp";
import {ArkadiumMenuComponent} from "../../components/arkadium-menu/ArkadiumMenuComponent.comp";
import {LoginButtonComponent} from "../../components/login-button/LoginButtonComponent.comp";
import {LogoComponent} from "../../components/logo/LogoComponent.comp";
import {BaseHeaderComponent} from "../BaseHeaderComponent";
import logoImg from "./assets/logo.png";
import {AdaptiveNavigationComponent} from "../../components/adaptive-navigation/AdaptiveNavigationComponent.comp";
import {CLIENT_MENU} from "./client-menu";
import {BurgerComponent} from "../../components/burger/BurgerComponent.comp";
import {RenderHelper} from "../../helpers/RenderHelper";


export class DailyMailHeaderComponent extends BaseHeaderComponent {

    /* ====== */
    /* RENDER */
    /* ====== */
    initComponent() {
        this.querySelector(AdaptiveNavigationComponent.componentId).setNav(CLIENT_MENU);
    }
    render() {
        return `
            <div class="header header-dailymail">
                <div class="row row-logo">
                    <img class="client-logo" src="${logoImg}" alt="dailymail">
                    ${this.renderComponent(AdaptiveNavigationComponent)}
                </div>
                <div class="row row-arkadium">
                    <div class="row space-between arkadium-body">
                        <div class="row">
                            ${this.renderComponent(ArkadiumMenuComponent)}
                            ${this.renderComponent(SearchComponent)}
                        </div>
                        ${this.renderComponent(LoginButtonComponent)}
                    </div>
                </div>
            </div>
        `;
    }

    initTabletComponent() {
        this.querySelector(BurgerComponent.componentId).setBody(this.renderBurgerContent());
    }
    renderTablet() {
        return `
            <div class="header header-dailymail header-tablet">
                <div class="row row-burger space-between">
                    <div class="row">
                        ${this.renderComponent(BurgerComponent)}
                        <a class="link-games" href="">Games</a>
                    </div>
                    ${this.renderComponent(LoginButtonComponent)}
                </div>
            </div>
        `;
    }

    renderBurgerContent() {
        return `
            ${RenderHelper.renderList("burger-body", CLIENT_MENU)}
        `;
    }

    initPhoneComponent() { return this.initTabletComponent(); }
    renderPhone() { return this.renderTablet(); }
}


window.customElements.define('dailymail-header-component', DailyMailHeaderComponent);