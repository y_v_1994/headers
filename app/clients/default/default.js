import "./default.scss";
import {SearchComponent} from "../../components/search/SearchComponent.comp";
import {ArkadiumMenuComponent} from "../../components/arkadium-menu/ArkadiumMenuComponent.comp";
import {LoginButtonComponent} from "../../components/login-button/LoginButtonComponent.comp";
import {LogoComponent} from "../../components/logo/LogoComponent.comp";
import {BaseHeaderComponent} from "../BaseHeaderComponent";

export class DefaultHeaderComponent extends BaseHeaderComponent {

    /* ====== */
    /* RENDER */
    /* ====== */
    render() {
        return `
            <div class="header header-default">
                <div class="row row-logo">
                    ${this.renderComponent(LogoComponent)}
                </div>
                <div class="row row-arkadium">
                    <div class="row">
                        ${this.renderComponent(ArkadiumMenuComponent)}
                        ${this.renderComponent(SearchComponent)}
                    </div>
                    ${this.renderComponent(LoginButtonComponent)}
                </div>
            </div>
        `;
    }
}

window.customElements.define('default-header-component', DefaultHeaderComponent);