export class RenderHelper {

    static renderListRecursive(className, list = []) {
        return `
                <ul class="${className}-list">
                    ${list.map((_) => this.renderListItemRecursive(className, _)).join('')}
                </ul>
            `;
    }

    static renderListItemRecursive(className, _) {
        return `
                <li class="${className}-list__item">
                    <a class="${className}-list__link" href="${_.href}">${_.title}</a>
                    ${_.items && _.items.length ? this.renderListRecursive(className, _.items) : ''}
                </li>
            `;
    }


    static renderList(className, list = []) {
        return `
                <ul class="${className}-list">
                    ${list.map((_) => this.renderListItem(className, _)).join('')}
                </ul>
            `;
    }

    static renderListItem(className, _) {
        return `
                <li class="${className}-list__item">
                    <a class="${className}-list__link" href="${_.href}">${_.title}</a>
                </li>
            `;
    }

}