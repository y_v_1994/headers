export class UtilHelper {

    constructor() {
        if (!Element.prototype.matches) {
            Element.prototype.matches = Element.prototype.msMatchesSelector || Element.prototype.webkitMatchesSelector;
        }
        if (!Element.prototype.closest) {
            Element.prototype.closest = function(s) {
                var el = this;
                if (!document.documentElement.contains(el))
                    return null;
                do {
                    if (el.matches(s))
                        return el;
                    el = el.parentElement || el.parentNode;
                } while (el !== null && el.nodeType === 1);return null;
            }
        }
        if (typeof window.CustomEvent !== "function") {
            function CustomEvent(event, params) {
                params = params || {
                        bubbles: false,
                        cancelable: false,
                        detail: undefined
                    };
                var evt = document.createEvent('CustomEvent');
                evt.initCustomEvent(event, params.bubbles, params.cancelable, params.detail);
                return evt;
            }
            CustomEvent.prototype = window.Event.prototype;
            window.CustomEvent = CustomEvent;
        }
        Math.easeInOutQuad = function(t, b, c, d) {
            t /= d / 2;
            if (t < 1)
                return c / 2 * t * t + b;
            t--;
            return -c / 2 * (t * (t - 2) - 1) + b;
        }
    }

    static debounce(func, wait, immediate) {
        var timeout;
        return function() {
            var context = this, args = arguments;
            var later = function() {
                timeout = null;
                if (!immediate) func.apply(context, args);
            };
            var callNow = immediate && !timeout;
            clearTimeout(timeout);
            timeout = setTimeout(later, wait);
            if (callNow) func.apply(context, args);
        };
    }

    static hasClass(el, className) {
        if (el.classList)
            return el.classList.contains(className);
        else
            return !!el.className.match(new RegExp('(\\s|^)' + className + '(\\s|$)'));
    }
    static addClass(el, className) {
        var classList = className.split(' ');
        if (el.classList)
            el.classList.add(classList[0]);
        else if (!UtilHelper.hasClass(el, classList[0]))
            el.className += " " + classList[0];
        if (classList.length > 1)
            UtilHelper.addClass(el, classList.slice(1).join(' '));
    }
    static removeClass(el, className) {
        var classList = className.split(' ');
        if (el.classList)
            el.classList.remove(classList[0]);
        else if (UtilHelper.hasClass(el, classList[0])) {
            var reg = new RegExp('(\\s|^)' + classList[0] + '(\\s|$)');
            el.className = el.className.replace(reg, ' ');
        }
        if (classList.length > 1)
            UtilHelper.removeClass(el, classList.slice(1).join(' '));
    }
    static toggleClass(el, className, bool) {
        if (bool)
            UtilHelper.addClass(el, className);
        else
            UtilHelper.removeClass(el, className);
    }
    static setAttributes(el, attrs) {
        for (var key in attrs) {
            el.setAttribute(key, attrs[key]);
        }
    }
    static getChildrenByClassName(el, className) {
        var children = el.children
          , childrenByClass = [];
        for (var i = 0; i < el.children.length; i++) {
            if (UtilHelper.hasClass(el.children[i], className))
                childrenByClass.push(el.children[i]);
            }
            return childrenByClass;
    }
    static is(elem, selector) {
        if (selector.nodeType) {
            return elem === selector;
        }
        var qa = (typeof (selector) === 'string' ? document.querySelectorAll(selector) : selector)
            , length = qa.length
            , returnArr = [];
        while (length--) {
            if (qa[length] === elem) {
                return true;
            }
        }
        return false;
    }
    static setHeight(start, to, element, duration, cb) {
        var change = to - start
            , currentTime = null;
        var animateHeight = function(timestamp) {
            if (!currentTime)
                currentTime = timestamp;
            var progress = timestamp - currentTime;
            var val = parseInt((progress / duration) * change + start);
            element.style.height = val + "px";
            if (progress < duration) {
                window.requestAnimationFrame(animateHeight);
            } else {
                cb();
            }
        };
        element.style.height = start + "px";
        window.requestAnimationFrame(animateHeight);
    }
    static scrollTo(final, duration, cb) {
        var start = window.scrollY || document.documentElement.scrollTop
            , currentTime = null;
        var animateScroll = function(timestamp) {
            if (!currentTime)
                currentTime = timestamp;
            var progress = timestamp - currentTime;
            if (progress > duration)
                progress = duration;
            var val = Math.easeInOutQuad(progress, start, final - start, duration);
            window.scrollTo(0, val);
            if (progress < duration) {
                window.requestAnimationFrame(animateScroll);
            } else {
                cb && cb();
            }
        };
        window.requestAnimationFrame(animateScroll);
    }
    static moveFocus(element) {
        if (!element)
            element = document.getElementsByTagName("body")[0];
        element.focus();
        if (document.activeElement !== element) {
            element.setAttribute('tabindex', '-1');
            element.focus();
        }
    }
    static getIndexInArray(array, el) {
        return Array.prototype.indexOf.call(array, el);
    }
    static cssSupports(property, value) {
        if ('CSS'in window) {
            return CSS.supports(property, value);
        } else {
            var jsProperty = property.replace(/-([a-z])/g, function(g) {
                return g[1].toUpperCase();
            });
            return jsProperty in document.body.style;
        }
    }
    static extend() {
        var extended = {};
        var deep = false;
        var i = 0;
        var length = arguments.length;
        if (Object.prototype.toString.call(arguments[0]) === '[object Boolean]') {
            deep = arguments[0];
            i++;
        }
        var merge = function(obj) {
            for (var prop in obj) {
                if (Object.prototype.hasOwnProperty.call(obj, prop)) {
                    if (deep && Object.prototype.toString.call(obj[prop]) === '[object Object]') {
                        extended[prop] = extend(true, extended[prop], obj[prop]);
                    } else {
                        extended[prop] = obj[prop];
                    }
                }
            }
        };
        for (; i < length; i++) {
            var obj = arguments[i];
            merge(obj);
        }
        return extended;
    }
    static osHasReducedMotion() {
        if (!window.matchMedia)
            return false;
        var matchMediaObj = window.matchMedia('(prefers-reduced-motion: reduce)');
        if (matchMediaObj)
            return matchMediaObj.matches;
        return false;
    }
}